﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bebidas
{
    public partial class Form1 : Form
    {
        private Copo generico;
        double liquido;
        public Form1()
        {
            InitializeComponent();

            ButtonAumentaQuantidade.Enabled = false;
            ButtonDiminuiQuantidade.Enabled = false;
            ButtonEncher.Enabled = false;
            ButtonEsvaziar.Enabled = false;

        }

        private void ButtonNovoCopo_Click(object sender, EventArgs e)
        {
            
            if (ComboBoxTamanhoCopo.SelectedItem == null)
            {
                LabelStatus.Text = "Escolhe um copo primeiro!";
            }
            else if (ComboBoxBebidas.SelectedItem == null)
            {
                LabelStatus.Text = "Se queres beber escolhe uma bebida!";
            }
            else
            {
                generico = new Copo(ComboBoxBebidas.SelectedItem.ToString(), Convert.ToDouble(ComboBoxTamanhoCopo.SelectedItem));
                LabelStatus.Text = generico.ToString();
                ButtonAumentaQuantidade.Enabled = true;
                ButtonDiminuiQuantidade.Enabled = true;
                ButtonEncher.Enabled = true;
                ButtonEsvaziar.Enabled = true;
                LabelQuantidade.Text = generico.Contem.ToString();
                liquido = generico.Contem;
            }


        }

        private void LabelStatus_Click(object sender, EventArgs e)
        {

        }

        private void ComboBoxBebidas_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (liquido > 0)
            {
                liquido--;
                LabelQuantidade.Text = liquido.ToString();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void ButtonAumentaQuantidade_Click(object sender, EventArgs e)
        {
          
            liquido++;
            LabelQuantidade.Text = liquido.ToString();

        }

        private void LabelQuantidade_Click(object sender, EventArgs e)
        {

        }

        private void ButtonEncher_Click(object sender, EventArgs e)
        {
            generico.Encher(liquido);
            LabelStatus.Text = generico.ToString();
            LabelPercentagem.Text = generico.ValorEmPercentagem().ToString() + "%";
            progressBar1.Value = generico.ValorEmPercentagem();
        }

        private void ButtonEsvaziar_Click(object sender, EventArgs e)
        {
            generico.Esvaziar(liquido);
            LabelStatus.Text = generico.ToString();
            LabelPercentagem.Text = generico.ValorEmPercentagem().ToString() + "%";
            progressBar1.Value = generico.ValorEmPercentagem();
        }


    }
}
