﻿namespace Bebidas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonNovoCopo = new System.Windows.Forms.Button();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.ComboBoxBebidas = new System.Windows.Forms.ComboBox();
            this.ComboBoxTamanhoCopo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ButtonAumentaQuantidade = new System.Windows.Forms.Button();
            this.ButtonDiminuiQuantidade = new System.Windows.Forms.Button();
            this.LabelQuantidade = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonEncher = new System.Windows.Forms.Button();
            this.ButtonEsvaziar = new System.Windows.Forms.Button();
            this.LabelPercentagem = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // ButtonNovoCopo
            // 
            this.ButtonNovoCopo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonNovoCopo.Location = new System.Drawing.Point(561, 20);
            this.ButtonNovoCopo.Name = "ButtonNovoCopo";
            this.ButtonNovoCopo.Size = new System.Drawing.Size(61, 51);
            this.ButtonNovoCopo.TabIndex = 0;
            this.ButtonNovoCopo.Text = "Novo Copo";
            this.ButtonNovoCopo.UseVisualStyleBackColor = true;
            this.ButtonNovoCopo.Click += new System.EventHandler(this.ButtonNovoCopo_Click);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(30, 37);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(22, 16);
            this.LabelStatus.TabIndex = 1;
            this.LabelStatus.Text = "- -";
            this.LabelStatus.Click += new System.EventHandler(this.LabelStatus_Click);
            // 
            // ComboBoxBebidas
            // 
            this.ComboBoxBebidas.FormattingEnabled = true;
            this.ComboBoxBebidas.Items.AddRange(new object[] {
            "Cerveja",
            "Coca-Cola",
            "Sumol"});
            this.ComboBoxBebidas.Location = new System.Drawing.Point(66, 140);
            this.ComboBoxBebidas.Name = "ComboBoxBebidas";
            this.ComboBoxBebidas.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxBebidas.TabIndex = 2;
            this.ComboBoxBebidas.SelectedIndexChanged += new System.EventHandler(this.ComboBoxBebidas_SelectedIndexChanged);
            // 
            // ComboBoxTamanhoCopo
            // 
            this.ComboBoxTamanhoCopo.FormattingEnabled = true;
            this.ComboBoxTamanhoCopo.Items.AddRange(new object[] {
            "50",
            "100",
            "150"});
            this.ComboBoxTamanhoCopo.Location = new System.Drawing.Point(98, 81);
            this.ComboBoxTamanhoCopo.Name = "ComboBoxTamanhoCopo";
            this.ComboBoxTamanhoCopo.Size = new System.Drawing.Size(66, 21);
            this.ComboBoxTamanhoCopo.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tamanho Copo";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Bebida";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(170, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "ml";
            // 
            // ButtonAumentaQuantidade
            // 
            this.ButtonAumentaQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonAumentaQuantidade.Location = new System.Drawing.Point(46, 318);
            this.ButtonAumentaQuantidade.Name = "ButtonAumentaQuantidade";
            this.ButtonAumentaQuantidade.Size = new System.Drawing.Size(36, 33);
            this.ButtonAumentaQuantidade.TabIndex = 7;
            this.ButtonAumentaQuantidade.Text = "+";
            this.ButtonAumentaQuantidade.UseVisualStyleBackColor = true;
            this.ButtonAumentaQuantidade.Click += new System.EventHandler(this.ButtonAumentaQuantidade_Click);
            // 
            // ButtonDiminuiQuantidade
            // 
            this.ButtonDiminuiQuantidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonDiminuiQuantidade.Location = new System.Drawing.Point(139, 318);
            this.ButtonDiminuiQuantidade.Name = "ButtonDiminuiQuantidade";
            this.ButtonDiminuiQuantidade.Size = new System.Drawing.Size(36, 33);
            this.ButtonDiminuiQuantidade.TabIndex = 8;
            this.ButtonDiminuiQuantidade.Text = "-";
            this.ButtonDiminuiQuantidade.UseVisualStyleBackColor = true;
            this.ButtonDiminuiQuantidade.Click += new System.EventHandler(this.button2_Click);
            // 
            // LabelQuantidade
            // 
            this.LabelQuantidade.AutoSize = true;
            this.LabelQuantidade.Location = new System.Drawing.Point(104, 330);
            this.LabelQuantidade.Name = "LabelQuantidade";
            this.LabelQuantidade.Size = new System.Drawing.Size(16, 13);
            this.LabelQuantidade.TabIndex = 9;
            this.LabelQuantidade.Text = "...";
            this.LabelQuantidade.Click += new System.EventHandler(this.LabelQuantidade_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(62, 286);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "Quantidade";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // ButtonEncher
            // 
            this.ButtonEncher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEncher.Location = new System.Drawing.Point(196, 318);
            this.ButtonEncher.Name = "ButtonEncher";
            this.ButtonEncher.Size = new System.Drawing.Size(75, 33);
            this.ButtonEncher.TabIndex = 11;
            this.ButtonEncher.Text = "Encher";
            this.ButtonEncher.UseVisualStyleBackColor = true;
            this.ButtonEncher.Click += new System.EventHandler(this.ButtonEncher_Click);
            // 
            // ButtonEsvaziar
            // 
            this.ButtonEsvaziar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEsvaziar.Location = new System.Drawing.Point(277, 318);
            this.ButtonEsvaziar.Name = "ButtonEsvaziar";
            this.ButtonEsvaziar.Size = new System.Drawing.Size(76, 33);
            this.ButtonEsvaziar.TabIndex = 12;
            this.ButtonEsvaziar.Text = "Esvaziar";
            this.ButtonEsvaziar.UseVisualStyleBackColor = true;
            this.ButtonEsvaziar.Click += new System.EventHandler(this.ButtonEsvaziar_Click);
            // 
            // LabelPercentagem
            // 
            this.LabelPercentagem.AutoSize = true;
            this.LabelPercentagem.Location = new System.Drawing.Point(493, 286);
            this.LabelPercentagem.Name = "LabelPercentagem";
            this.LabelPercentagem.Size = new System.Drawing.Size(15, 13);
            this.LabelPercentagem.TabIndex = 14;
            this.LabelPercentagem.Text = "%";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(381, 318);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(241, 33);
            this.progressBar1.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 384);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.LabelPercentagem);
            this.Controls.Add(this.ButtonEsvaziar);
            this.Controls.Add(this.ButtonEncher);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LabelQuantidade);
            this.Controls.Add(this.ButtonDiminuiQuantidade);
            this.Controls.Add(this.ButtonAumentaQuantidade);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxTamanhoCopo);
            this.Controls.Add(this.ComboBoxBebidas);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.ButtonNovoCopo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonNovoCopo;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.ComboBox ComboBoxBebidas;
        private System.Windows.Forms.ComboBox ComboBoxTamanhoCopo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ButtonAumentaQuantidade;
        private System.Windows.Forms.Button ButtonDiminuiQuantidade;
        private System.Windows.Forms.Label LabelQuantidade;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ButtonEncher;
        private System.Windows.Forms.Button ButtonEsvaziar;
        private System.Windows.Forms.Label LabelPercentagem;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

